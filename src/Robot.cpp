/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include <iostream>
#include <string>

#include <Drive/DifferentialDrive.h>
#include <Joystick.h>
#include <SampleRobot.h>
#include <SmartDashboard/SendableChooser.h>
#include <SmartDashboard/SmartDashboard.h>
#include <Spark.h>
#include <Victor.h>
#include <Timer.h>
#include <WPILib.h>
#include "Mechanisms.h"
#include <Encoder.h>
#include <vector>
#include <DigitalInput.h>

using namespace frc;

/**
 * This is a demo program showing the use of the DifferentialDrive class.
 * The SampleRobot class is the base of a robot application that will
 * automatically call your Autonomous and OperatorControl methods at the right
 * time as controlled by the switches on the driver station or the field
 * controls.
 *
 * WARNING: While it may look like a good choice to use for your code if you're
 * inexperienced, don't. Unless you know what you are doing, complex code will
 * be much more difficult under this system. Use IterativeRobot or Command-Based
 * instead if you're new.
 */
class Robot : public SampleRobot {
public:
	Robot() :
		m_leftMotor(new Talon(0)),
		m_rightMotor(new Talon(1)),
		m_robotDrive(*m_leftMotor, *m_rightMotor)

		m_shoulderMotor(new Spark(2)),
		m_shoulderLowerLimit(new DigitalInput(11)),
		m_shoulderMiddleLimit(new DigitalInput(13)),
		m_shoulderUpperLimit(new DigitalInput(15)),

		m_wristMotor(new Spark(3)),
		m_wristStartLimit(new DigitalInput(2)),
		m_wristSquareLimit(new DigitalInput(3)),
		m_wristObtuseLimit(new DigitalInput(4)),
		m_wristStraightLimit(new DigitalInput(5)),

		m_gripperMotor(new Spark(8)),
		m_gripperPiston(new Piston(new Solenoid(0,2), new Solenoid(0,3), Piston::EXTENDED)),
		m_gripperLimit(new DigitalInput(0)),

		m_lightStripController(new Spark(9))
	{

		hangerIsExtended = false;
		m_leftEncoder = new Encoder(7,6,false, Encoder::EncodingType::k4X);
		m_rightEncoder = new Encoder(9,8,false, Encoder::EncodingType::k4X);
		m_leftPIDController = new PIDController(0.1, 0.001, 0.0, m_leftEncoder, m_leftMotor);
		m_rightPIDController = new PIDController(0.1, 0.001, 0.0, m_rightEncoder, m_rightMotor);
		// m_gyro = new frc::AnalogGyro(0);

		// Note SmartDashboard is not initialized here, wait until
		// RobotInit to make SmartDashboard calls
		m_robotDrive.SetExpiration(0.1);

		lifter = new Lifter(
			m_shoulderMotor, m_shoulderLowerLimit, m_shoulderMiddleLimit, m_shoulderUpperLimit,
			m_wristMotor, m_wristStartLimit, m_wristSquareLimit, m_wristObtuseLimit, m_wristStraightLimit)

		shoulder = new Shoulder(m_shoulderMotor, m_shoulderLowerLimit, m_shoulderMiddleLimit, m_shoulderUpperLimit);
		wrist = new Wrist(m_wristMotor, m_wristStartLimit, m_wristSquareLimit, m_wristObtuseLimit, m_wristStraightLimit);
		arm = new Arm(shoulder, wrist);
		gripper = new Gripper(m_gripperMotor, m_gripperPiston, m_gripperLimit);
		hanger = new Piston(new Solenoid(0,0), new Solenoid(0,1), Piston::RETRACTED);
		lightstrip = new LightStrip(m_lightStripController);
	}

	void RobotInit() {

		m_leftEncoder->SetSamplesToAverage(5);
		m_rightEncoder->SetSamplesToAverage(5);
		m_leftEncoder->SetDistancePerPulse(1.0/360.0 * 19.25);
		m_rightEncoder->SetDistancePerPulse(1.0/360.0 * 19.25);
		m_leftEncoder->SetMinRate(1.0);
		m_rightEncoder->SetMinRate(1.0);
//		m_gyro->SetSensitivity(kVoltsPerDegreePerSecond);

		m_chooser.AddDefault(kAutoNameDefault, kAutoNameDefault);
		//m_chooser.AddObject(kAutoNameCustom, kAutoNameCustom);
		m_chooser.AddObject(kAutoLeft1, kAutoLeft1);
		m_chooser.AddObject(kAutoRight1, kAutoRight1);
		m_chooser.AddObject(kAutoMiddle1, kAutoMiddle1);
		SmartDashboard::PutData("Auto Modes", &m_chooser);
	}

	/*
	 * This autonomous (along with the chooser code above) shows how to
	 * select between different autonomous modes using the dashboard. The
	 * sendable chooser code works with the Java SmartDashboard. If you
	 * prefer the LabVIEW Dashboard, remove all of the chooser code and
	 * uncomment the GetString line to get the auto name from the text box
	 * below the Gyro.
	 *
	 * You can add additional auto modes by adding additional comparisons to
	 * the if-else structure below with additional strings. If using the
	 * SendableChooser make sure to add them to the chooser code above as
	 * well.
	 */
	void Autonomous() {

		std::string gameMessage = ds.GetGameSpecificMessage();

		std::string autoSelected = m_chooser.GetSelected();
		// std::string autoSelected = frc::SmartDashboard::GetString("Auto Selector", kAutoNameDefault);
		std::cout << "Auto selected: " << autoSelected << std::endl;

		// MotorSafety improves safety when motors are updated in loops
		// but is disabled here because motor updates are not looped in
		// this autonomous mode.
		m_robotDrive.SetSafetyEnabled(false);

		lightstrip->SetPattern(LightStrip::SOLIDYELLOW);

		m_leftPIDController->Enable();
		m_rightPIDController->Enable();

//		m_leftPIDController->SetSetpoint(60.0);
//		m_rightPIDController->SetSetpoint(60.0);
//
//		Wait(3.0);

		m_leftPIDController->Reset();
		m_rightPIDController->Reset();



		if (autoSelected == kAutoLeft1) {
			// Custom Auto goes here
			std::cout << "Running custom Autonomous" << std::endl;

			// Spin at half speed for two seconds
//			m_robotDrive.ArcadeDrive(0.0, 0.5);
//			frc::Wait(2.0);
//
//			// Stop robot
//			m_robotDrive.ArcadeDrive(0.0, 0.0);

			m_leftPIDController->SetSetpoint(60.0);
			m_rightPIDController->SetSetpoint(30.0);

			Wait(3.0);

			m_leftPIDController->SetSetpoint(-5.0);
			m_rightPIDController->SetSetpoint(5.0);

//			while (IsAutonomous() && IsEnabled());
//			{
//				shoulder->Update();
//				wrist->Update();
//			}
		}
		else if (autoSelected == kAutoRight1) {
//			while (IsAutonomous() && IsEnabled())
//			{
//				shoulder->Update();
//				wrist->Update();
//			}
		}
		else if (autoSelected == kAutoMiddle1) {
			if (gameMessage[0] == 'L')
			{
				m_leftPIDController->SetSetpoint(3.0);
				m_rightPIDController->SetSetpoint(5.0);
				Wait(2.0);
				m_leftPIDController->Reset();
				m_leftPIDController->SetSetpoint(3.0);
				Wait(1.0);
			}
			else
			{

			}
		}
		else {
			// Default Auto goes here
			std::cout << "Running default Autonomous" << std::endl;

			m_robotDrive.ArcadeDrive(1.0, 0.0);
			Wait(1.0);

			m_robotDrive.ArcadeDrive(0.5, 0.0);
			Wait(1.0);

			m_robotDrive.ArcadeDrive(-1.0, 0.0);
			Wait(0.5);

			m_robotDrive.ArcadeDrive(0.0, 0.0);
		}

	}

	/*
	 * Runs the motors with arcade steering.
	 */
	void OperatorControl() override {

		m_robotDrive.SetSafetyEnabled(true);
		while (IsOperatorControl() && IsEnabled()) {

			// Drive with arcade style (use right stick)
			m_robotDrive.ArcadeDrive(-rightJoystick.GetY(), rightJoystick.GetX());

			ControlArm()
			ControlGripper();
			ControlHanger();
			ControlArm();
			ShowLights();
			UpdateSmartDashboard();

			// The motors will be updated every 5ms
			Wait(0.005);
		}
	}

	/*
	 * Runs during test mode
	 */
	void Test() override
	{
		m_robotDrive.SetSafetyEnabled(true);
		while (IsTest() && IsEnabled()) {
			
			ControlArm();
			ControlGripper();
			ControlHanger();
			ControlArm();
			ShowLights();
			UpdateSmartDashboard();
			Wait(0.005);
		}
	}

	void ControlGripper()
	{
		if (rightJoystick.GetRawButtonPressed(4))
		{
			gripper->Close();
		}
		if (rightJoystick.GetRawButtonPressed(5))
		{
			gripper->Open();
		}

		Gripper::Direction direction = Gripper::Direction::STOP;
		if (rightJoystick.GetRawButtonPressed(6))
		{
			direction = Gripper::Direction::SUCK;
		}
		else if (rightJoystick.GetRawButtonPressed(7))
		{
			direction = Gripper::Direction::SPIT;
		}
		else if (rightJoystick.GetRawButtonPressed(8))
		{
			direction = Gripper::Direction::STOP;
		}

		gripper->SetDesiredDirection(direction);
		gripper->Update();
	}

	void ControlHanger()
	{
		if(rightJoystick.GetRawButtonPressed(10))
		{
			hanger->Retract();
			hangerIsExtended = false;
		}
		else if(rightJoystick.GetRawButtonPressed(11))
		{
			if(arm->GetLastPosition() == Arm::HANG)
			{
				hanger->Extend();
				hangerIsExtended = true;
			}
		}
	}

	void ControlArm()
	{
		if(xboxController.GetBackButton())
		{
			// manual - but allow hang instead?
			lifter->Manual(xboxController.GetRawAxis(1), xboxController.GetAButton());
		}
		else if(xboxController.GetStartButton())
		{
			// Move the shoulder down and the wrist up
			lifter->Manual(-0.5, 1);
		}
		else
		{
			lifter->Automatic(xboxController.GetRawAxis(1), xboxController.GetAButton());
		}

		// double yAxis1 = xboxController.GetRawAxis(1);
		// double yAxis2 = xboxController.GetRawAxis(5);
		// double rightTrigger = xboxController.GetAxis(XboxController::JoystickHand::kRightHand);
		// double leftTrigger = xboxController.GetAxis(XboxController::JoystickHand::kLeftHand);
		// m_shoulderMotor->Set(-yAxis1);
		// m_wristMotor->Set(yAxis2);

	}

	void ControlComplexArm()
	{
		double rightTriggerAxis = xboxController.GetTriggerAxis(XboxController::JoystickHand::kRightHand);
		double fuel = rightTriggerAxis < 0 ? 0 : rightTriggerAxis > 1 ? 1 : rightTriggerAxis;

		Arm::Position armTargetPosition;
		bool updateArmTargetPosition = false;
		if (xboxController.GetAButton()) // A(1)
		{
			armTargetPosition = Arm::Position::PICKUP;
			updateArmTargetPosition = true;
		}
		if (xboxController.GetBButton()) // B(2)
		{
			armTargetPosition = Arm::Position::SWITCHDELIVERY;
			updateArmTargetPosition = true;
		}
		if (xboxController.GetXButton()) // X(3)
		{
			armTargetPosition = Arm::Position::SCALEDELIVERY;
			updateArmTargetPosition = true;
		}
		if (xboxController.GetYButton()) // Y(4)
		{
			armTargetPosition = Arm::Position::HANG;
			updateArmTargetPosition = true;
		}
		if  (xboxController.GetStartButton()) // START(8)
		{
			armTargetPosition = Arm::Position::START;
			updateArmTargetPosition = true;
		}

		if(hangerIsExtended)
		{
			fuel = 0;
		}
		wrist->SetFuel(fuel * 0.5);
		shoulder->SetFuel(fuel);
		if(updateArmTargetPosition)
		{
			arm->SetTargetPosition(armTargetPosition);
		}

		arm->Update();
	}

	void ShowLights()
	{
		if(ds.GetMatchTime() >= 145)
		{
			lightstrip->SetPattern(LightStrip::COLORWAVESRAINBOW);
		}
		else if(ds.GetMatchTime() >= 120)
		{
			lightstrip->SetPattern(LightStrip::STROBEGOLD);
		}
		else if(ds.GetAlliance()==DriverStation::Alliance::kBlue)
		{
			lightstrip->SetPattern(LightStrip::SOLIDBLUE);
		}
		else
		{
			lightstrip->SetPattern(LightStrip::SOLIDRED);
		}
	}

	void UpdateSmartDashboard()
	{
		SmartDashboard::PutNumber("Encoder Left Distance", m_leftEncoder->GetDistance());
		SmartDashboard::PutNumber("Encoder Right Distance", m_rightEncoder->GetDistance());
		SmartDashboard::PutNumber("Encoder Left Rate", m_leftEncoder->GetRate());
		SmartDashboard::PutNumber("Encoder Right Rate", m_rightEncoder->GetRate());

		SmartDashboard::PutBoolean("Shoulder Lower Limit", m_shoulderLowerLimit->Get());
		SmartDashboard::PutBoolean("Shoulder Middle Limit", m_shoulderMiddleLimit->Get());
		SmartDashboard::PutBoolean("Shoulder Upper Limit", m_shoulderUpperLimit->Get());
		SmartDashboard::PutBoolean("Wrist Start Limit", m_wristStartLimit->Get());
		SmartDashboard::PutBoolean("Wrist Square Limit", m_wristSquareLimit->Get());
		SmartDashboard::PutBoolean("Wrist Obtuse Limit", m_wristObtuseLimit->Get());
		SmartDashboard::PutBoolean("Wrist Straight Limit", m_wristStraightLimit->Get());
		SmartDashboard::PutBoolean("Gripper Limit", m_gripperLimit->Get());
	}

private:
	// Robot drive system
	Talon* m_leftMotor;
	Talon* m_rightMotor;

	// Shoulder system
	Spark* m_shoulderMotor;
	DigitalInput* m_shoulderLowerLimit;
	DigitalInput* m_shoulderMiddleLimit;
	DigitalInput* m_shoulderUpperLimit;

	// Wrist system
	Spark* m_wristMotor;
	DigitalInput* m_wristStartLimit;
	DigitalInput* m_wristSquareLimit;
	DigitalInput* m_wristObtuseLimit;
	DigitalInput* m_wristStraightLimit;

	// Gripper system
	Spark* m_gripperMotor;
	Piston* m_gripperPiston;
	DigitalInput m_gripperLimit;

	// Light strip system
	Spark* m_lightStripController;

	Shoulder* shoulder;
	Wrist* wrist;
	Arm* arm;
	Gripper* gripper;
	LightStrip* lightstrip;
	Piston* hanger;
	bool hangerIsExtended;
	Lifter* lifter;

	//frc::Preferences prefs;
	DriverStation& ds = DriverStation::GetInstance();

	Joystick rightJoystick{0};
	Joystick leftJoystick{1};
	XboxController xboxController{2};

	frc::SendableChooser<std::string> m_chooser;
	const std::string kAutoNameDefault = "Default";
//	const std::string kAutoNameCustom = "My Auto";
	const std::string kAutoLeft1 = "Auto Left 1";
	const std::string kAutoRight1 = "Auto Right 1";
	const std::string kAutoMiddle1 = "Auto Middle 1";

	static constexpr double kVoltsPerDegreePerSecond = 0.0128;
	Encoder* m_leftEncoder;
	Encoder* m_rightEncoder;
	DifferentialDrive m_robotDrive;
	PIDController* m_leftPIDController;
	PIDController* m_rightPIDController;
//	AnalogGyro* m_gyro;
};

START_ROBOT_CLASS(Robot)
