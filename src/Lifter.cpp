/*
 * Lifter.cpp
 *
 *  Created on: Feb 3, 2018
 *      Author: Robotics
 */

#include <Lifter.h>

static const bool LIMIT_CLOSED = false;
static const bool LIMIT_OPEN = true;

Lifter::Lifter(Spark* shoulderMotor,
		DigitalInput* shoulderLowerLimit,
		DigitalInput* shoulderMiddleLimit,
		DigitalInput* shoulderUpperLimit,
		Spark* wristMotor,
		DigitalInput* wristStartLimit,
		DigitalInput* wristSquareLimit,
		DigitalInput* wristObtuseLimit,
		DigitalInput* wristStraightLimit) {
	m_shoulderMotor = shoulderMotor;
	m_shoulderLowerLimit = shoulderLowerLimit;
	m_shoulderMiddleLimit = shoulderMiddleLimit;
	m_shoulderUpperLimit = shoulderUpperLimit;
	m_wristMotor = wristMotor;
	m_wristStartLimit = wristStartLimit;
	m_wristSquareLimit = wristSquareLimit;
	m_wristObtuseLimit = wristObtuseLimit;
	m_wristStraightLimit = wristStraightLimit;

	m_lastWristPosition = WristPosition::START;
	m_lastShoulderPosition = ShoulderPosition::DOWN;
}

Lifter::~Lifter() {
	// TODO Auto-generated destructor stub
}

// Lifter::Position Lifter::GetTargetPosition() const
// {
// 	return m_desiredPosition;
// }

// Lifter::Position Lifter::GetLastPosition() const
// {
// 	return m_lastPosition;
// }

// void Lifter::SetTargetPosition(Position pos)
// {
// 	m_desiredPosition = pos;
// }

void Lifter::Update()
{
	if (m_shoulderLowerLimit->Get() == LIMIT_CLOSED)
		m_lastShoulderPosition = ShoulderPosition::DOWN;
	if (m_shoulderMiddleLimit->Get() == LIMIT_CLOSED)
		m_lastShoulderPosition = ShoulderPosition::MIDDLE;
	if (m_shoulderUpperLimit->Get() == LIMIT_CLOSED)
		m_lastShoulderPosition = ShoulderPosition::UP;

	if (m_wristStartLimit->Get() == LIMIT_CLOSED)
		m_lastWristPosition = WristPosition::START;
	if (m_wristSquareLimit->Get() == LIMIT_CLOSED)
		m_lastWristPosition = WristPosition::MIDDLE;
	if (m_wristObtuseLimit->Get() == LIMIT_CLOSED)
		m_lastWristPosition = WristPosition::OBTUSE;
	if (m_wristStraightLimit->Get() == LIMIT_CLOSED)
		m_lastWristPosition = WristPosition::STRAIGHT;
}

void Lifter::Automatic(double direction, bool ignorePosition)
{
	Update();
	double nextShoulderSpeed = 0;
	double nextWristSpeed = 0;
	if (direction > 0.1)
	{
		if(m_lastShoulderPosition == ShoulderPosition::DOWN)
		{
			// Between lowest point and moving up
			switch(m_lastWristPosition)
			{
				case WristPosition::START:
					nextWristSpeed = -direction;
					nextShoulderSpeed = direction;
					break;
				case WristPosition::SQUARE:
					nextShoulderSpeed = direction;
					break;
				case WristPosition::OBTUSE: 
					nextWristSpeed = direction;
					nextShoulderSpeed = -direction;
					break;
				case WristPosition::STRAIGHT:
					nextWristSpeed = direction;
					nextShoulderSpeed = -direction;
					break;
				default:
					break;
			}
		}
		else if(m_lastShoulderPosition == ShoulderPosition::MIDDLE)
		{
			// Between middle point and moving up
			switch(m_lastWristPosition)
			{
				case WristPosition::START:
					nextWristSpeed = -direction;
					nextShoulderSpeed = direction;
					break;
				case WristPosition::SQUARE:
					nextWristSpeed = -direction;
					nextShoulderSpeed = direction;
					break;
				case WristPosition::OBTUSE:
					nextShoulderSpeed = direction;
					break;
				case WristPosition::STRAIGHT:
					nextWristSpeed = direction;
					nextShoulderSpeed = direction;
					break;
				default:
					break;
			}
		}
		else if(m_lastShoulderPosition == ShoulderPosition::UP)
		{
			switch(m_lastWristPosition)
			{
				case WristPosition::START:
					nextWristSpeed = -direction;
					break;
				case WristPosition::SQUARE:
					nextWristSpeed = -direction;
					break;
				case WristPosition::OBTUSE:
					nextWristSpeed = direction;
					break;
				case WristPosition::STRAIGHT:
					nextWristSpeed = direction;
					break;
				default:
					break;
			}
		}
	}
	else if (direction < 0.1)
	{
		if(m_lastShoulderPosition == ShoulderPosition::DOWN)
		{
			switch(m_lastWristPosition)
			{
				case WristPosition::START:
					break;
				case WristPosition::SQUARE:
					break;
				case WristPosition::OBTUSE:
					break;
				case WristPosition::STRAIGHT:
					break;
				default:
					break;
			}
		}
		else if(m_lastShoulderPosition == ShoulderPosition::MIDDLE)
		{
			switch(m_lastWristPosition)
			{
				case WristPosition::START:
					break;
				case WristPosition::SQUARE:
					break;
				case WristPosition::OBTUSE:
					break;
				case WristPosition::STRAIGHT:
					break;
				default:
					break;
			}
		}
		else if(m_lastShoulderPosition == ShoulderPosition::UP)
		{
			switch(m_lastWristPosition)
			{
				case WristPosition::START:
					break;
				case WristPosition::SQUARE:
					break;
				case WristPosition::OBTUSE:
					break;
				case WristPosition::STRAIGHT:
					break;
				default:
					break;
			}
		}

		if(m_lastShoulderPosition == ShoulderPosition::UP)
		{
			if(m_lastWristPosition == WristPosition::OBTUSE || m_lastWristPosition == WristPosition::OBTUSE)
			{
				nextWristSpeed = direction;
			}
			else
			{
				nextShoulderSpeed = direction;
			}
		}
	}

	this->Manual(nextShoulderSpeed, nextWristSpeed);
}

void Lifter::Manual(double shoulderDirection, double wristDirection)
{
	// Full range of shoulder
	if (shoulderDirection > 0.1)
	{
		if (m_shoulderUpperLimit->Get() == LIMIT_CLOSED)
			shoulderDirection = 0;
	}
	else if (shoulderDirection < -0.1)
	{
		if (m_shoulderLowerLimit->Get() == LIMIT_CLOSED)
			shoulderDirection = 0;
	}

	// Full range of wrist 
	if (wristDirection > 0.1)
	{
		if (m_wristStartLimit->Get() == LIMIT_CLOSED)
			wristDirection = 0;
	}
	else if (wristDirection < 0.1)
	{
		if (m_wristStraightLimit->Get() == LIMIT_CLOSED)
			wristDirection = 0;
	}

	m_shoulderMotor->Set(shoulderDirection);
	m_wristMotor->Set(wristDirection);

}
