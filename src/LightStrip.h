/*
 * LightStrip.h
 *
 *  Created on: Feb 10, 2018
 *      Author: Robotics
 */

#ifndef SRC_LIGHTSTRIP_H_
#define SRC_LIGHTSTRIP_H_

#include <Spark.h>

class LightStrip {
public:
// http://www.revrobotics.com/content/docs/REV-11-1105-UM.pdf
	LightStrip(Spark* spark);
	virtual ~LightStrip();

	enum Pattern
	{
		SOLIDRED = 0,
		STROBEBLUE,
		TWINKLESGREEN,
		COLORWAVESRAINBOW,
		SOLIDBLACK,
		SOLIDYELLOW,
		STROBERED,
		SOLIDBLUE,
		STROBEGOLD,

		PatternCount
	};

	void SetPattern(Pattern pattern);

private:

	static constexpr float patterns [PatternCount] =
	{
			0.61,
			-0.09,
			-0.47,
			-0.45,
			0.99,
			0.69,
			-0.11,
			0.87,
			-0.07
	};

	Spark* m_spark;
};

#endif /* SRC_LIGHTSTRIP_H_ */
