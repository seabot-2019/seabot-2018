/*
 * Wrist.cpp
 *
 *  Created on: Jan 27, 2018
 *      Author: Robotics
 */

#include <Wrist.h>

static const bool LIMIT_CLOSED = false;

Wrist::Wrist(Spark* motor,
		  DigitalInput* startLimit,
		  DigitalInput* squareLimit,
		  DigitalInput* obtuseLimit,
		  DigitalInput* straightLimit) {
	m_motor = motor;
	m_startLimit = startLimit;
	m_squareLimit = squareLimit;
	m_obtuseLimit = obtuseLimit;
	m_straightLimit = straightLimit;
	m_lastPosition = Position::START;
	m_targetPosition = Position::START;
	m_fuel = 0;
}

Wrist::~Wrist() {
	// TODO Auto-generated destructor stub
}

Wrist::Position Wrist::GetTargetPosition() const
{
	return m_targetPosition;
}

Wrist::Position Wrist::GetLastPosition() const
{
	return m_lastPosition;
}

void Wrist::SetFuel(double fuel)
{
	m_fuel = fuel;
}

void Wrist::SetTargetPosition(Position pos)
{
	m_targetPosition = pos;
}

void Wrist::Update()
{
	//Updates status using limit switches
	if (m_startLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::START;
	}
	else if (m_squareLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::SQUARE;
	}
	else if (m_obtuseLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::OBTUSE;
	}
	else if (m_straightLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::STRAIGHT;
	}
	else
	{

	}

	if (m_lastPosition != m_targetPosition) // if not at correct position
	{
		// move arm to new position
		float speed;
		if (m_targetPosition > m_lastPosition)
		{
			// TODO: figure out motor direction from build team
			speed = 1;
		}
		else
		{
			speed = -1;
		}
		m_motor->Set(speed * m_fuel);

		// determine target limit
		DigitalInput* limitSwitch = nullptr;
		if (m_targetPosition == START)
		{
			limitSwitch = m_startLimit;
		}
		else if (m_targetPosition == SQUARE)
		{
			limitSwitch = m_squareLimit;
		}
		else if (m_targetPosition == OBTUSE)
		{
			limitSwitch = m_obtuseLimit;
		}
		else if (m_targetPosition == STRAIGHT)
		{
			limitSwitch = m_straightLimit;
		}

		// if limit switch hit
		if (limitSwitch->Get()==LIMIT_CLOSED)
		{
			m_motor->Set(0);
		}
	}
}
