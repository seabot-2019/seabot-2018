/*
 * Claw.h
 *
 *  Created on: Jan 27, 2018
 *      Author: Robotics
 */

#include <Spark.h>
#include <DigitalInput.h>
#include "Piston.h"

using namespace frc;

#ifndef SRC_GRIPPER_H_
#define SRC_GRIPPER_H_

class Gripper {
public:
	typedef enum Status
	{
		OPENED,
		CLOSED
	} Status;
	typedef enum Direction
	{
		SUCK,
		SPIT,
		STOP
	} Direction;
	Gripper(Spark* motor,
		 Piston* piston,
		 DigitalInput* boxLimit,
		 float speed = 0.5f);
	virtual ~Gripper();

	void Open();
	void Close();
	void SetDesiredDirection(Direction direction);
	void Update();

private:
	Spark* m_motor;
	Piston* m_piston;
	DigitalInput* m_boxLimit;
	float m_speed;
	Status m_status;
	Direction m_direction;
	Direction m_desiredDirection;
};

#endif /* SRC_GRIPPER_H_ */
