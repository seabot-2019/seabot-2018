/*
 * LightStrip.cpp
 *
 *  Created on: Feb 10, 2018
 *      Author: Robotics
 */

#include <LightStrip.h>
#include <Spark.h>

constexpr float LightStrip::patterns [PatternCount];

LightStrip::LightStrip(Spark* spark) {
	m_spark = spark;
}

LightStrip::~LightStrip() {
	// TODO Auto-generated destructor stub
}

void LightStrip::SetPattern(Pattern pattern)
{
	m_spark->Set(patterns[pattern]);
}
