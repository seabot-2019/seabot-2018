/*
 * Arm.cpp
 *
 *  Created on: Jan 27, 2018
 *      Author: Robotics
 */

#include <Shoulder.h>

static const bool LIMIT_CLOSED = false;

Shoulder::Shoulder(Spark* motor,
		DigitalInput* downLimit,
		DigitalInput* middleLimit,
		DigitalInput* upLimit) {
	m_motor = motor;
	m_downLimit = downLimit;
	m_middleLimit = middleLimit;
	m_upLimit = upLimit;
	m_lastPosition = Position::DOWN;
	m_targetPosition = Position::DOWN;
	m_fuel = 0;
}

Shoulder::~Shoulder() {
	// TODO Auto-generated destructor stub
}

Shoulder::Position Shoulder::GetTargetPosition() const
{
	return m_targetPosition;
}

Shoulder::Position Shoulder::GetLastPosition() const
{
	return m_lastPosition;
}

void Shoulder::SetFuel(double fuel)
{
	m_fuel = fuel;
}

void Shoulder::SetTargetPosition(Position pos)
{
	m_targetPosition = pos;
}

void Shoulder::Update()
{
	//Updates status using limit switches
	if (m_downLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::DOWN;
	}
	else if (m_middleLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::MIDDLE;
	}
	else if (m_upLimit->Get()==LIMIT_CLOSED)
	{
		m_lastPosition = Position::UP;
	}
	else
	{

	}

	if (m_lastPosition != m_targetPosition)
	{
		float speed;
		if (m_targetPosition > m_lastPosition)
		{
			speed = 1;
		}
		else
		{
			speed = -1;
		}
		m_motor->Set(speed * m_fuel);

		DigitalInput* limitSwitch = nullptr;
		if (m_targetPosition == DOWN)
		{
			limitSwitch = m_downLimit;
		}
		else if (m_targetPosition == MIDDLE)
		{
			limitSwitch = m_middleLimit;
		}
		else if (m_targetPosition == UP)
		{
			limitSwitch = m_upLimit;
		}

		if (limitSwitch->Get()==LIMIT_CLOSED)
		{
			m_motor->Set(0);
		}

	}
}
