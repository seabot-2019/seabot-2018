/*
 * Claw.cpp
 *
 *  Created on: Jan 27, 2018
 *      Author: Robotics
 */

#include <Gripper.h>
#include <DigitalInput.h>

static const bool LIMIT_CLOSED = false;

Gripper::Gripper(Spark* motor,
		   Piston* piston,
		   DigitalInput* boxLimit,
		   float speed) {
	m_motor = motor;
	m_piston = piston;
	m_boxLimit = boxLimit;
	m_speed = speed;
}

Gripper::~Gripper() {
	// TODO Auto-generated destructor stub
}

void Gripper::Open() {
	m_piston->Retract();
	m_status = OPENED;
}

void Gripper::Close() {
	m_piston->Extend();
	m_status = CLOSED;
}

void Gripper::SetDesiredDirection(Direction direction) {
	m_desiredDirection = direction;
}

void Gripper::Update() {
	if(m_desiredDirection == SPIT)
	{
		m_direction = SPIT;
		m_motor->Set(-m_speed);
	}
	else if(m_desiredDirection == SUCK)
	{
		if(m_boxLimit->Get() == LIMIT_CLOSED)
		{
			m_direction = STOP;
			m_motor->Set(0);
		}
		else
		{
			m_direction = SUCK;
			m_motor->Set(m_speed);
		}
	}
	else
	{
		m_direction = STOP;
		m_motor->Set(0);
	}
}
