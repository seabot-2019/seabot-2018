/*
 * Piston.h
 *
 *  Created on: Jan 28, 2017
 *      Author: Robotics
 */

#ifndef SRC_PISTON_H_
#define SRC_PISTON_H_

#include "Solenoid.h"

using namespace frc;

class Piston {
public:
	typedef enum Status
	{
		RETRACTED,
		EXTENDED
	} Status;

	Piston(Solenoid* pExtend,
		   Solenoid* pRetract,
		   Status status);
	~Piston();

	void Extend();
	void Retract();
	Status GetStatus();

private:
	//Piston();
	Solenoid* m_pExtend;
	Solenoid* m_pRetract;
	Status m_status;
};

#endif /* SRC_PISTON_H_ */
