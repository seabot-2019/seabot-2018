/*
 * Arm.h
 *
 *  Created on: Jan 27, 2018
 *      Author: Robotics
 */

#include <DigitalInput.h>
#include <Spark.h>

using namespace frc;

#ifndef SRC_SHOULDER_H_
#define SRC_SHOULDER_H_

class Shoulder {
public:
	typedef enum Position
	{
		DOWN		= 0,
		MIDDLE		= 1,
		UP			= 2
	} Position;
	Shoulder(Spark* motor,
		DigitalInput* downLimit,
		DigitalInput* middleLimit,
		DigitalInput* upLimit);
	virtual ~Shoulder();

	Position GetTargetPosition() const;
	Position GetLastPosition() const;
	void SetFuel(double fuel);
	void SetTargetPosition(Position pos);
	void Update();
private:
	Spark* m_motor;
	DigitalInput* m_downLimit;
	DigitalInput* m_middleLimit;
	DigitalInput* m_upLimit;
	Position m_targetPosition;
	Position m_lastPosition;
	double m_fuel;
};

#endif /* SRC_SHOULDER_H_ */
