/*
 * Arm.h
 *
 *  Created on: Feb 17, 2018
 *      Author: Robotics
 */

#ifndef SRC_ARM_H_
#define SRC_ARM_H_

#include "Shoulder.h"
#include "Wrist.h"

class Arm {
public:
	typedef enum Position
	{
		PICKUP			= 0,
		START			= 1,
		SWITCHDELIVERY	= 2,
		HANG			= 3,
		SCALEDELIVERY	= 4
	} Position;
	Arm(Shoulder* shoulder,
		Wrist* wrist);
	virtual ~Arm();

	Position GetTargetPosition() const;
	Position GetLastPosition () const;
	Position GetNextPosition () const;
	void SetTargetPosition(Position pos);
	void Update();
private:
	Shoulder* m_shoulder;
	Wrist* m_wrist;
	Position m_targetPosition;
	Position m_lastPosition;
	Position m_nextPosition;
};

#endif /* SRC_ARM_H_ */
