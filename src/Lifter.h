/*
 * Lifter.h
 *
 *  Created on: Feb 3, 2018
 *      Author: Robotics
 */

#ifndef SRC_LIFTER_H
#define SRC_LIFTER_H

#include <Shoulder.h>
#include "Wrist.h"

class Lifter {
public:

	typedef enum WristPosition
	{
		START 		= 0,
		SQUARE  	= 1,
		OBTUSE 		= 2,
		STRAIGHT	= 3
	} WristPosition;
	typedef enum ShoulderPosition
	{
		DOWN		= 0,
		MIDDLE		= 1,
		UP			= 2
	} ShoulderPosition;

	// typedef enum Position
	// {
	// 	START,
	// 	PICKUP,
	// 	SWITCHDELIVERY,
	// 	SCALEDELIVERY,
	// } Position;
	// typedef enum Status
	// {
	// } Status;
	Lifter(
		Spark* shoulderMotor,
		DigitalInput* shoulderLowerLimit,
		DigitalInput* shoulderMiddleLimit,
		DigitalInput* shoulderUpperLimit,
		Spark* wristMotor,
		DigitalInput* wristStartLimit,
		DigitalInput* wristSquareLimit,
		DigitalInput* wristObtuseLimit,
		DigitalInput* wristStraightLimit);
	virtual ~Lifter();
	// Position GetTargetPosition() const;
	// Position GetLastPosition() const;
	// void SetTargetPosition(Position pos);
	//void Update();
	void Automatic(double direction, bool ignorePosition);
	void Manual(double shoulderDirection, double wristDirection)
private:
	void Update();
	Spark* m_shoulderMotor,
	DigitalInput* m_shoulderLowerLimit,
	DigitalInput* m_shoulderMiddleLimit,
	DigitalInput* m_shoulderUpperLimit,
	Spark* m_wristMotor,
	DigitalInput* m_wristStartLimit,
	DigitalInput* m_wristSquareLimit,
	DigitalInput* m_wristObtuseLimit,
	DigitalInput* m_wristStraightLimit
	WristPosition m_lastWristPosition;
	ShoulderPosition m_lastShoulderPosition;
	// Position m_lastPosition;
	// Position m_desiredPosition;
	// Position m_nextPosition;
};

#endif /* SRC_LIFTER_H */
