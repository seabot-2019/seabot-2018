/*
 * Piston.cpp
 *
 *  Created on: Jan 28, 2017
 *      Author: Robotics
 */

#include <Piston.h>

Piston::Piston(Solenoid* pExtend,
		       Solenoid* pRetract,
			   Piston::Status status) {
	m_pExtend = pExtend;
	m_pRetract = pRetract;
	m_status = status;

}

Piston::~Piston() {
}

void Piston::Extend()
{
	m_pExtend->Set(true);
	m_pRetract->Set(false);
	m_status = Piston::EXTENDED;
}

void Piston::Retract()
{
	m_pRetract->Set(true);
	m_pExtend->Set(false);
	m_status = Piston::RETRACTED;
}

Piston::Status Piston::GetStatus()
{
	return m_status;
}
