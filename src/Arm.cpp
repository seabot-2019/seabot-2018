/*
 * Arm.cpp
 *
 *  Created on: Feb 17, 2018
 *      Author: Robotics
 */

#include <Arm.h>

Arm::Arm(Shoulder* shoulder,
		Wrist* wrist) {
	m_shoulder = shoulder;
	m_wrist = wrist;
	m_lastPosition = Position::START;
	m_targetPosition = Position::START;
	m_nextPosition = Position::START;
}

Arm::~Arm() {
	// TODO Auto-generated destructor stub
}

Arm::Position Arm::GetTargetPosition() const
{
	return m_targetPosition;
}

Arm::Position Arm::GetLastPosition() const
{
	return m_lastPosition;
}

Arm::Position Arm::GetNextPosition() const
{
	return m_nextPosition;
}

void Arm::SetTargetPosition(Position pos)
{
	m_targetPosition = pos;
}

void Arm::Update()
{
	Shoulder::Position shoulderLastPosition = m_shoulder->GetLastPosition();
	Wrist::Position wristLastPosition = m_wrist->GetLastPosition();
	if (shoulderLastPosition == Shoulder::Position::DOWN)
	{
		switch (wristLastPosition)
		{
			case Wrist::Position::START:
				m_lastPosition = START;
				break;
			case Wrist::Position::OBTUSE:
				m_lastPosition = PICKUP;
				break;
		}
	}

	if (shoulderLastPosition == Shoulder::Position::MIDDLE)
	{
		switch (wristLastPosition)
		{
		case Wrist::Position::SQUARE:
			m_lastPosition = SWITCHDELIVERY;
			break;
		case Wrist::Position::OBTUSE: //Penalty position
			break;
		case Wrist::Position::START:
			break;
		}
	}

	if (shoulderLastPosition == Shoulder::Position::UP)
	{
		switch (wristLastPosition)
		{
		case Wrist::Position::STRAIGHT:
			m_lastPosition = HANG;
			break;
		case Wrist::Position::OBTUSE:
			m_lastPosition = SCALEDELIVERY;
			break;
		case Wrist::Position::SQUARE:
			break;
		case Wrist::Position::START:
			break;
		}
	}

	if (m_lastPosition != m_targetPosition)
	{
		if (m_targetPosition > m_lastPosition)
		{
			m_nextPosition = static_cast<Position>(m_lastPosition + 1);
		}
		else
		{
			m_nextPosition = static_cast<Position>(m_lastPosition - 1);
		}
	}

	if (m_lastPosition != m_nextPosition)
	{
		Shoulder::Position shoulderNextPosition;
		Wrist::Position wristNextPosition;

		switch (m_lastPosition)
		{
		case PICKUP:
			shoulderNextPosition = Shoulder::Position::DOWN;
			wristNextPosition = Wrist::Position::OBTUSE;
			break;
		case START:
			shoulderNextPosition = Shoulder::Position::DOWN;
			wristNextPosition = Wrist::Position::START;
			break;
		case SWITCHDELIVERY:
			shoulderNextPosition = Shoulder::Position::MIDDLE;
			wristNextPosition = Wrist::Position::SQUARE;
			break;
		case HANG:
			shoulderNextPosition = Shoulder::Position::UP;
			wristNextPosition = Wrist::Position::STRAIGHT;
			break;
		case SCALEDELIVERY:
			shoulderNextPosition = Shoulder::Position::UP;
			wristNextPosition = Wrist::Position::OBTUSE;
			break;
		}

		m_wrist->SetTargetPosition(wristNextPosition);
		m_shoulder->SetTargetPosition(shoulderNextPosition);
		m_wrist->Update();
		m_shoulder->Update();

	}
}
