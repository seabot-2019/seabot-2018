/*
 * Wrist.h
 *
 *  Created on: Jan 27, 2018
 *      Author: Robotics
 */

#include <DigitalInput.h>
#include <Spark.h>

using namespace frc;

#ifndef SRC_WRIST_H_
#define SRC_WRIST_H_

class Wrist {
public:
	typedef enum Position
	{
		START 		= 0,
		SQUARE  	= 1,
		OBTUSE 		= 2,
		STRAIGHT	= 3
	} Position;
	Wrist(Spark* motor,
		  DigitalInput* startLimit,
		  DigitalInput* squareLimit,
		  DigitalInput* obtuseLimit,
		  DigitalInput* straightLimit);
	virtual ~Wrist();

	void SetFuel(double fuel);
	Position GetTargetPosition() const;
	Position GetLastPosition() const;
	void SetTargetPosition(Position pos);
	void Update();
private:
	Spark* m_motor;
	DigitalInput* m_startLimit;
	DigitalInput* m_squareLimit;
	DigitalInput* m_obtuseLimit;
	DigitalInput* m_straightLimit;
	Position m_targetPosition;
	Position m_lastPosition;
	double m_fuel;
};

#endif /* SRC_WRIST_H_ */_
